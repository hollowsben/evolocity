"use strict";

var ROUNDING_DP = 2;

/*
** Evolocity Website Main JS
** By Ben Hollows
*/

// POLYFILL

// From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
if (!Object.keys) {
	Object.keys = (function() {
		'use strict';
		var hasOwnProperty = Object.prototype.hasOwnProperty,
			hasDontEnumBug = !({ toString: null }).propertyIsEnumerable('toString'),
			dontEnums = [
				'toString',
				'toLocaleString',
				'valueOf',
				'hasOwnProperty',
				'isPrototypeOf',
				'propertyIsEnumerable',
				'constructor'
			],
			dontEnumsLength = dontEnums.length;

		return function(obj) {
			if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
				throw new TypeError('Object.keys called on non-object');
			}

			var result = [], prop, i;

			for (prop in obj) {
				if (hasOwnProperty.call(obj, prop)) {
					result.push(prop);
				}
			}

			if (hasDontEnumBug) {
				for (i = 0; i < dontEnumsLength; i++) {
					if (hasOwnProperty.call(obj, dontEnums[i])) {
						result.push(dontEnums[i]);
					}
				}
			}
			return result;
		};
	}());
}

// END POLYFILL

var ev = {	//global object
	interval: {},
	updateInterval: 15000,	//15s
	lastUpdate: new Date(),	//html returned by server contains current data
	isUpdating: false,
	fullscreen: {},
	modal: null,
	bkoffset: 0,
	competition_data: {},
};

var util = {	//small utility functions
	setUpdateInterval: function(i) {
		if (ev.interval.update) {
			clearInterval(ev.interval.update);
		}
		ev.updatePeriod = i;
		ev.interval.update = setInterval(function(){
			update("all", 10);
		}, i);
	}
};

$(document).ready(function() {
	leaderboardBackgroundSetup();
	$(window).on("resize", leaderboardBackgroundSetup);

	ev.interval.status = setInterval(function () {
		updateStatus();
	}, 100);	//0.15s

	ev.interval.update = setInterval(function(){
		update("all", 10);
	}, ev.updateInterval);

	$("[id^=results] h2").on("click", function() {
		fullscreen($(this).parent().attr("id"));
	});

	//modal background close
	$(".modal").on("click", closeModal);
	$(".modal .modal-content").on("click", function (e) {
		e.stopPropagation();
	});

	update();
});

function updateStatus() {
	var html;

	if (ev.isUpdating) {
		html = $("<span data-status='good'>Updating</span>");

		if ((ev.lastUpdate.getTime() + ev.updateInterval + 5000) < (new Date().getTime())) { //updating for over 5s past interval
			html.attr("data-status", "bad").append(" | No message recieved for " + timeparse.relative(ev.lastUpdate, null, false))
		}
	} else {
		html = "Last update: " + timeparse.relative(ev.lastUpdate);
	}

	$("#connection-info").html(html);
}

function error(obj) {
	var type = typeof(obj);
	if (type == "string") {
		alert(obj);
		return true;
	}

	if (type == "object" && obj.status == "err") {
		alert(obj.response);
		return true;
	}

	return false;
}

function update() {
	ev.isUpdating = true;
	$.get("/teams", function(response) {
		var team_data = {};
		response.forEach(function(team) {
			team_data[team.team_id] = team;
		})

		ev.competition_data.teams = team_data;
		$.get("/scores", applyUpdate);
	});
}

function applyUpdate(data) {
	ev.isUpdating = false;

	if (error(data)) {	return; }
	ev.lastUpdate = new Date();

	var new_main = $("<main>");

	// Cache data
	var cdata = data;

	// Order category names by order
	var categories = Object.keys(cdata.meta);
	categories.sort(function(a, b) {
		if (cdata.meta[a].display_order < cdata.meta[b].display_order) {
			return -1;
		}
		if (cdata.meta[a].display_order > cdata.meta[b].display_order) {
			return 1;
		}
		return 0;
	});

	//loop through categories
	categories.forEach(function(key) {
		var meta = cdata.meta[key];
		var data = cdata.results[key];

		if (data.length === 0) {
			// Do not display categories with no data
			return;
		}

		var table = buildTable(data, meta);

		$("<section>")
			.attr("id", "results-" + meta.category_name)
			.append(
				$("<h2>").html(meta.display_name)
			)
			.append(
				$("<table class='results'>").html(table)
			)
			.appendTo(new_main);
	});

	if (new_main.html() === "") {
		new_main.html("<section><h2>No results yet!</h2></section>");
	}

	$("main").html(new_main.html());
}

function buildTable(data, meta) {
	var table = $("<table>");

	// Order fields
	var order = [];
	// Ignore fields with diplay_order of -1
	meta.fields.forEach(function (field){
		if (field.display_order !== -1) {
			order.push(field);
		}
	});
	//Sort fields by display_order
	order.sort(function(a, b) {
		return a.display_order - b.display_order;
	});

	// Table headings
	var hdrow = $("<tr>");
	// Default headings
	hdrow
		.append($("<th>Ranking</th>"))
		.append($("<th>Team</th>"));
	// Category headings
	order.forEach(function(field) {
		hdrow.append(
			$("<th>").text(field.display_name)
		);
	});
	table.append(hdrow);

	// Sort results by place
	var results = [];

	// Data may come as an array or an object
	if (Array.isArray(data)) {
		data.forEach(function(elm, index) {
			results.push({team_id: index, results:elm});
		});
	} else {
		for (var team_id in data) {
			results.push({team_id: team_id, results:data[team_id]});
		}
	}

	// Sort results
	results.sort(function(a, b){
		if (meta.sort_direction === "ASC") {
			return a.results[meta.sort_field] - b.results[meta.sort_field];
		} else {
			return b.results[meta.sort_field] - a.results[meta.sort_field];
		}
	});


	// Table body
	results.forEach(function(result, index){
		var row = $("<tr>");

		// Place
		row.append($("<td>").html(index + 1));

		//Team
		row.append($("<td>").html(ev.competition_data.teams[result.team_id].name));

		order.forEach(function(elm){
			var field_name = elm.field_name;

			var value = result.results[field_name];

			if (typeof(value) === "number") {
				var multiplier = 10 * ROUNDING_DP;
				value = Math.floor(value * multiplier) / multiplier;
			}
			// var unit = meta.units[key];
			// if (!unit) {
			 	var unit = "";
			// } else {
			// 	//parse unit superscript;
			//
			// 	//encode value
			// 	var unittext = $('<div/>').text(unit).html();
			// 	//replace ^ with <sup>. note capture groups are not zero indexed!
			// 	unit = unittext.replace(/\^(.)/g, "<sup>$1</sup>");
			// }

			//encode value (html required for unit superscript)
			//var currenthtml = $('<div/>').text(current[key]).html();

			row.append($("<td>").html(value + unit));
		});
		table.append(row)
	});

	return table.html();
}

function fullscreen(id) {
	if (ev.fullscreen.id) {	//if element is fullscreen
		$("body, main, #" + ev.fullscreen.id).removeClass("fullscreen"); //clear all styles
			$("#" + id).css("top", "");
		ev.fullscreen.id = null;
		$("body").scrollTop(ev.fullscreen.lastpos);
		return;
	}

	if (typeof(id) != "string") {
		return false;
	}

	ev.fullscreen.lastpos = $("body").scrollTop();
	$("body").scrollTop(0);

	$("body, main, #" + id).addClass("fullscreen");
	$("#" + id).css("top", $("header").height() + "px");
	ev.fullscreen.id = id;

}

function closeModal() {
	ev.modal.css("display", "none");
	ev.modal = null;
}

function openModal(elm) {
	elm.css("display", "flex");
	ev.modal = elm;
}

function teamModal(teamID) {
	if (ev.modal != null) {
		closeModal();
	}
	//getdata

	openModal($("#teaminfo"));

	//image fix
	$("#teaminfo .modal-content .modal-body img").hide();
	$("#teaminfo .modal-content .modal-body img").height($("#teaminfo .modal-content .modal-body div").height());
	$("#teaminfo .modal-content .modal-body img").show();
}

function leaderboardBackgroundSetup() {
	$("#bkoffset").html(''); //clear styling

	$("section").each(function (index, elm) {
		//get values
		var elmtop = $(elm).position().top,
			elmleft = $(elm).position().left;

		$("#bkoffset").append(".background-offset-" + index + "::before{background-position:-" + elmleft + "px -"+ elmtop + "px}");

		$(elm).removeClass(function(index, css) {
			return (css.match (/(^|\s)background-offset-\S+/g) || []).join(' ');
		});

		$(elm).addClass("background-offset-" + index);
	});
}
