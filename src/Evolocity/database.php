<?php
namespace Evolocity;

require_once("src/Evolocity/config.php");

/**
 * Class for managing database connections and queries
 *
 * Options:
 * - noData: if set values are not bound to the statement
 * - schema: the schema to execute the statement on
 * - returnPrimaryKey: if set the primary key of the inserted row is returned
 */
class Query {
	/** @var array $paramaters Holds paramaters specified when class constructed */
	private $paramaters;

	/**
	 * Constuctor for Query
	 *
	 * @param string $sql SQL statement to be executed with variables replaced by '?'.
	 * @param array $values The first element is a string specifing the types of the following elements in the array: "siiss" specifies string, number, number, string, string. The following elements will replace question marks '?' in the statement in order of appearance.
	 * @param array $options Options for the query. (Optional)
	 *
	 * @returns array[] Results of query
	 */
	public function __construct($sql, $values, $options = []) {
		$this->paramaters = [
			"sql" => $sql,
			"data" => $values,
			"options" => $options,
		];
	}

	/**
	 * Helper function for PHP 5.3+, converts arrays to reference arrays
	 *
	 * @param array $arr Array to converts
	 * @return array Converted array
	 */
	protected function refValues($arr){
	    if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
	    {
	        $refs = array();
	        foreach($arr as $key => $value)
	            $refs[$key] = &$arr[$key];
	        return $refs;
	    }
	    return $arr;
	}

	/**
	 * Checks whether the option has been set
	 *
	 * @param string $optionName Option to check
	 * @return boolean True if specified option has been set
	 */
	private function optionSet($optionName) {
		return !empty($this->paramaters["options"][$optionName]);
	}

	/**
	 * Returns the value of an option
	 *
	 * @param string $optionName Option to check
	 * @return mixed Value of specified option
	 */
	private function getOption($optionName) {
		return $this->paramaters["options"][$optionName];
	}

	/** @var \mysqli_stmt 	Stmt object of query */
	private $stmt;

	/** @var \mysqli_result Query result metadata */
	private $meta;

	/** @var mixed 			ID of inserted row (only if specified in options) */
	private $id;

	/** @var array 			Result of query */
	private $results;

	/** @var Error 			$error 		Last error encountered */
	private $error;

	/**
	 * Returns Stmt object
	 * @return \mysqli_stmt Stmt object
	 */
	public function getStmt() {
		return $this->stmt;
	}

	/**
	 * Returns meta object
	 * @return \mysqli_result Result Metadata
	 */
	public function getMeta() {
		return $this->meta;
	}

	/**
	 * Returns primary keyof inserted row (requires option)
	 * @return mixed Primary Key
	 */
	public function getInsertedPrimaryKey() {
		return $this->id;
	}

	/**
	 * Returns result of query
	 * @return array Results
	 */
	public function getResults() {
		return $this->results;
	}

	/**
	 * Returns latest error
	 * @return Error error
	 */
	public function getError() {
		return $this->error;
	}

	/**
	 * Static function to check if a schema exists on the server
	 * @param string $schema The schema to check for
	 * @return boolean Whether the schema exists
	 */
	public static function schemaExists($schema) {
		// Get database configuration
		$config = (new Config())["server"]["database"];

		// Connect to database
		$conn = new \mysqli(
			$config["host"],
			$config["username"],
			$config["password"]
		);

		// Prepare statement
		$stmt = $conn->prepare("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = ?");
		// If error occured when preparing statement return error
		if ($stmt == false) {
			return new Error("db01", $conn->error, "Query::doesSchemaExist");
		}

		$stmt->bind_param('s', $schema);

		// Execute statement and store result
		$stmt->execute();
		$stmt->store_result();

		// Close connection to database
		$conn->close();

		// Return true if there is a result
		return ($stmt->num_rows >= 1);
	}

	/**
	 * Executes the query
	 *
	 * @return array|mixed The result of the query or the primary key of the inserted row (if specified in options)
	 */
	public function execute() {
		// Get database configuration
		$config = (new Config())["server"]["database"];

		// Connect to database
		$conn = new \mysqli(
			$config["host"],
			$config["username"],
			$config["password"],
			$config["schema"]
		);

		// Prepare statement
		$this->stmt = $conn->prepare($this->paramaters["sql"]);
		// If error occured when preparing statement return error
		if ($this->stmt == false) {
			$this->error = new Error("db01", $conn->error, $this);
			return $this->error;
		}

		// If data exists (noData not set) - bind paramaters to statment
		if (!$this->optionSet("noData")) {
			call_user_func_array(array($this->stmt, 'bind_param'), $this->refValues($this->paramaters["data"]));
		}

		// Execute statement and store result
		$s = $this->stmt->execute();
		$this->stmt->store_result();

		// Close connection to database
		$conn->close();

		// Check for error in result of query and reuturn error if present
		$this->meta = $this->stmt->result_metadata();
		if (!is_object($this->meta) or !is_object($this->stmt)) {
			$this->error = new Error("db02", $s, $this);
			return $this->error;
		}

		// If option returnPrimaryKey set return primary key of inserted row
		if ($this->optionSet("returnPrimaryKey")) {
			$this->id = $this->stmt->insert_id;
			return $this->id;
		}

		// Bind result to statement
		while ($column = $this->meta->fetch_field()) {
		   $bindVarsArray[] = &$results[$column->name];
		}
		call_user_func_array(array($this->stmt, 'bind_result'), $bindVarsArray);

		// Transform output to array and return
		$output = array();
		while ($this->stmt->fetch()) {
			$row = [];

			// Break reference in columns
			foreach($results as $name => $column) {
				$row[$name] = $column;
			}

			$output[] = $row;
		}
		$this->results = $output;
		return $output;
	}
}
