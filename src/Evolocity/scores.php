<?php
namespace Evolocity;

require_once("src/Evolocity/objects.php");
require_once("src/Evolocity/database.php");
require_once("src/Evolocity/category.php");
require_once("src/Evolocity/teams.php");

class ScoreInterface {
	static function getResults() {
		$scores = [];
		$meta = [];
		foreach (Category::_list() as $category_def) {
			$category_name = $category_def["category_name"];
			$category = new Category($category_name);
			$scores[$category_name] = $category->getResults();
			$meta[$category_name] = $category->getMeta();
		}

		$results = [
			"results" => $scores,
			"meta" => $meta
		];

		return $results;
	}

	static function updateResults($data) {
		$now = \DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
		$timestamp =$now->format("Y-m-d H:i:s.u");

		if (isset($data["team_rfid"])) {
			$data["team_id"] = TeamInterface::rfidToId($data["team_rfid"]);
			if ($data["team_id"] === null) {
				return '{"status": "ERROR: Unknown team!"}';
			}
		}

		foreach ($data["fields"] as $field) {
			if (isset($data["team_rfid"])) {
				$timestamp = $field["data"];
				$field["data"] = 0;
			}
			
			$insert = new Query(
				"INSERT INTO data (timestamp, category_name, field_name, team_id, data)
				 VALUES (?, ?, ?, ?, ?)",
				 ["sssis", $timestamp, $data["category_name"], $field["field_name"], $data["team_id"], $field["data"]]
			);
			$insert->execute();
		}
		
		return '{"status": "ok"}';
	}
}
?>

