<?php
namespace Evolocity;

/**
 * Error class returned by Evolocity functions and classes on error condition, designed to be passed to user.
 */
class Error {
	/** @var string[] Descriptions of possible errors */
	private $errorList = [
		"db01" => "Could not prepare SQL query",
		"db02" => "SQL query failed",
	];

	/** @var string Error Code */
	public $code;

	/** @var string Error Description */
	public $description;

	/** @var mixed Data provided with error */
	public $data;

	/** @var object Object that created error */
	public $from;

	/**
	 * Constructor for Error
	 *
	 * @param string $code Error Code
	 * @param mixed $data Data provided with error
	 * @param object $from Object that created error (generally self)
	 */
	public function __construct($code, $data = null, $from = null) {
		$this->code = $code;
		$this->data = $data;
		$this->from = $from;
		$this->description = $this->errorList[$code];
	}
}
?>
