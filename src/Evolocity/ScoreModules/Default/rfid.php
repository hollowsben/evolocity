<?php
namespace Evolocity\ScoreModules;

class RFID implements Field {
	protected $meta;
	protected $data;
	protected $options;

	public function __construct($meta, $data, $computed_results) {
		$options = json_decode($meta["options"], True);
		$this->meta = $meta;
		$this->options = $options;
		$this->data = $data;
	}

	public function getValue() {
		// Get shortest lap

		$d = 1;
		$shortest_lap = null;
		foreach ($this->data as $index => $row) {
			if ($index == 0) {
				continue;
			}

			$prev_lap = $this->data[$index - 1];
			$prev_time = new \DateTime($prev_lap["timestamp"]);
			$cur_time = new \DateTime($row["timestamp"]);
			$time_diff = $prev_time->diff($cur_time)->format("%I:%S");

			if ($shortest_lap == null or $time_diff < $shortest_lap) {
				$shortest_lap = $time_diff;
			}
		}

		// Get total time
		if (count($this->data) < $this->options["laps"] + 1) {
			$total_time = "- - : - -";
		} else {
			$initial_time = new \DateTime($this->data[0]["timestamp"]);
			$end_time = new \DateTime($this->data[$this->options["laps"]]["timestamp"]);
			$total_time = $initial_time->diff($end_time)->format("%I:%S");
		}

		$value = [
			"laps" => count($this->data) - 1,
			"shortest_lap" => $shortest_lap,
			"total_time" => $total_time,
		];
		return json_encode($value);
	}

	public function isComplete() {
		// Return false is there is no data
		if (count($this->data) == 0) {
			return False;
		}

		// Return false if less than required laps completed
		//if (count($this->data) == $this->options["laps"] + 1) {
		//	return False;
		//}

		return True;
	}
}

class RFID_Laps implements Field {
	protected $meta;
	protected $data;

	public function __construct($meta, $data, $computed_results) {
		$this->meta = $meta;
		$this->data = $computed_results;
	}

	public function getValue() {
		$rfid_data = json_decode($this->data["rfid"], true);
		return $rfid_data["laps"];
	}

	public function isComplete() {
		return (isset($this->data["rfid"]));
	}
}

class RFID_Shortest_Lap implements Field {
	protected $meta;
	protected $data;

	public function __construct($meta, $data, $computed_results) {
		$this->meta = $meta;
		$this->data = $computed_results;
	}

	public function getValue() {
		$rfid_data = json_decode($this->data["rfid"], true);
		return $rfid_data["shortest_lap"];
	}

	public function isComplete() {

		return (isset($this->data["rfid"]));
	}
}

class RFID_Total_Time implements Field {
	protected $meta;
	protected $data;

	public function __construct($meta, $data, $computed_results) {
		$this->meta = $meta;
		$this->data = $computed_results;
	}

	public function getValue() {
		$rfid_data = json_decode($this->data["rfid"], true);
		return $rfid_data["total_time"];
	}

	public function isComplete() {

		return (isset($this->data["rfid"]));
	}
}

class RFID_Total_Time_Seconds implements Field {
	protected $meta;
	protected $data;

	public function __construct($meta, $data, $computed_results) {
		$this->meta = $meta;
		$this->data = $computed_results;
	}

	public function getValue() {
		$rfid_data = json_decode($this->data["rfid"], true);
		$time = explode(':', $rfid_data["total_time"]);
		return (((int)$time[0]) * 60) + (int)$time[1];
	}

	public function isComplete() {
		return (isset($this->data["rfid"]));
		if (isset($this->data["rfid"])) {
			return ($this->getValue() != "- - : - -");
		}
		return False;
	}
}

?>
