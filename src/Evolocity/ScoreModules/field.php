<?php
namespace Evolocity\ScoreModules;

/**
 * Interface for getting information on the state of a field
 */
interface Field {
	/**
	 * Constructor for a Field
	 *
	 * @param mixed $meta Field type data from category configuration
	 * @param array $data Array of data rows from the database pertaining to the instance of the field
	 * @param array $computed_results Results of previously computed fields
	 */
	public function __construct($meta, $data, $computed_results);

	/**
	 * Returns the value of the field, or null if the field is not complete
	 * @return float|null Value of the field, or null if the field is not complete
	 */
	public function getValue();

	/**
	 * Checks wether there is enough data to compute the final value of the field
	 * @return boolean Whether the field is complete
	 */
	public function isComplete();
}
?>
