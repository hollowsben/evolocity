<?php
namespace Evolocity;

/**
 * Loads and provides configuration for the application
 *
 * As ArrayAccess is partially implemented (offsetGet, offsetGet), configurations can be retrieved by using the Config object as an array
 * E.g.
 * $config = new Config[]
 * $db_condig = $config['db_config']
 *
 * isset() and empty() will also work
 *
 * offsetSet (array[] = x) and offsetUnset (unset()) are implemented but unused to prevent dynamic modification of loaded configuration
 */
class Config implements \ArrayAccess {
	/**
	 * Stores configuration, as this variable is static it can be accessed by all instances of Config
	 * @var array Configuration
	 */
	private static $config = array();

	/**
	 * Loads the configuration fron a YAML file and stores in configuration
	 *
	 * @param string $name Key under which to store loaded configuration
	 * @param string $path Path to load configuration from, relative to document_root/config
	 *
	 * @return boolean True on success, False on failure
	 */
	public function load($name, $path) {
		$config_file = file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/config/" . $path);
		if ($config_file == False) {
			//Error report
			return False;
		}

		$config_parsed = \yaml_parse($config_file);
		if ($config_parsed == False) {
			//Error report
			return False;
		}

		self::$config[$name] = $config_parsed;
		return True;
	}

	/**
	 * Returns the specified configuration
	 *
	 * @param string $name Name of configuration to return
	 * @return mixed[]|null Configuration, or null if configuration not loaded
	 */
	public function get($name) {
		return isset(self::$config[$name]) ? self::$config[$name] : null;
	}

	/**
	 * Checks if specified configuration is loaded
	 *
	 * @param string $name Name of configuration to check
	 * @return boolean True if configuration loaded, False if not
	 */
	public function isLoaded($name) {
		return isset(self::$config[$name]);
	}

	// ---- ArrayAccess Methods ----

	/**
	 * Implements ArrayAccess and allows accessing configuration in arraylike manner: (new Config())[$offset]
	 *
	 * @param string $offset Name of configuration to return
	 * @return mixed[]|null Configuration, or null if configuration not loaded
	 */
	public function offsetGet($offset) {
		return isset(self::$config[$offset]) ? self::$config[$offset] : null;
	}

	/**
	 * Implements ArrayAccess and allows checking if configuration loaded with isset() and empty()
	 *
	 * @param string $offset Name of configuration to check
	 * @return boolean True if configuration loaded, False if not
	 */
	public function offsetExists($offset) {
		return isset(self::$config[$offset]);
	}

	// The config should not be changed dynamically, thus the following functions will do nothing and should not be documented

	/** @ignore */
	public function offsetSet($offset, $value) {
		return;
	}

	/** @ignore */
	public function offsetUnset($offset) {
		return;
	}

}


?>
