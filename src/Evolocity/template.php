<?php
/*

*/
namespace Evolocity;

/**
 * Class for a html template
 *
 * Template format:
 * HTML file
 * Commands syntax {{{var}}}
 * eg. <span>{{{title}}}</span> would insert $vars['title'];
 *
 * Variables can be strings or functions
 *
 * Errors in inseting data are output as HTML comments
 */
class Template {
	/** @var string HTML contained in template file */
	public $html;

	/** @var string[]|callable[] The data to insert into the template*/
	public $vars;

	/** Constructor for Template
	  *
	  * @param string $file Name of template file relative to [document_root]/templates/
	  * @param string[]|callable[]|void Data to insert into template, can be ommitted
	  */
	public function __construct($file, $vars = []) {
		$this->html = file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/templates/" . $file . ".html");
		$this->vars = $vars;
	}

	/** Parses HTML and inserts data
	 *
	 * @param string[]|callable[]|void Data to insert into template (omit paramater to use currently stored data)
	 * @return string HTML of template with data inserted
	 */
	public function parse($vars = null) {
		// If $vars unset use exisiting data
		if ($vars === null) {
			$vars = $this->vars;
		}

		// Search for any text in triple curly brackets {{{ }}} and replace with data
		return preg_replace_callback("/{{{(.*)}}}/", function($matches) use ($vars) {
			// Check there is a value for the variable
			if (!isset($vars[$matches[1]])) {
				return "<!-- Templating error - could not find value for " . $matches[0] . " -->";
			}

			$var_value = $vars[$matches[1]];

			if (is_string($var_value)) {
				return $var_value;
			}

			if (is_callable($var_value)) {
				return $var_value();
			}

			// Data is of unsupported type, output error as HTML comment
			return "<!-- Templating error - could not find value for " . $matches[0] . " -->";

		}, $this->html);
	}
}
?>
