<?php
namespace Evolocity;

require_once("src/Evolocity/database.php");

class TeamInterface {
	static function rfidToId($rfid) {
		$query = new Query(
			"SELECT team_id
			 FROM teams
			 WHERE rfid = ?",
			 ['s', $rfid]
		);
		$team = $query->execute();

		if (count($team) == 0) {
			return null;
		}

		return $team[0]["team_id"];
	}

	static function updateTeams($data, $operation) {
		switch ($operation) {
			case "insert":
				$insert = new Query(
					"INSERT INTO teams (race_number, name, vehicle_type, custom_controller, class, rfid, members)
					 VALUES (?, ?, ?, ?, ?, ?, ?)",
					 ["ississs", $data["race_number"], $data["name"], $data["vehicle_type"], $data["custom_controller"],
					 	$data["class"], $data["rfid"], $data["members"]]
				);
				$insert->execute();
				break;

			case "update":
				$update = new Query(
					"UPDATE teams
						SET race_number=?, name=?, vehicle_type=?, custom_controller=?, class=?, rfid=?, members=?
						WHERE team_id = ?",
					["ississsi", $data["race_number"], $data["name"], $data["vehicle_type"], $data["custom_controller"],
						$data["class"], $data["rfid"], $data["members"], $data["team_id"]]
				);
				$update->execute();
				break;

			case "delete":
				$query = new Query(
					"DELETE FROM teams WHERE team_id = ?",
					["i", $data["team_id"]]
				);
				$query->execute();
				break;
		}
	}

	static function getTeams() {
		// Get Teams
		$teams_query = new Query(
			"SELECT team_id, race_number, name, vehicle_type, custom_controller, class, members, rfid
			 FROM teams",
			 [],
			 ["noData" => true]
		);
		$teams = $teams_query->execute();

		return $teams;
	}
}
?>
