<?php
//----------todo case for supreme award

namespace Evolocity;

require_once "src/Evolocity/scores.php";
require_once "src/Evolocity/teams.php";

/**
 * Generates HTML serverside for non js clients
 */
class Render {
	/** Returns HTML for index page leaderboards*/
	static function indexLeaderboards() {

		$scores = ScoreInterface::getResults();
		$teams_data = TeamInterface::getTeams();

		$teams = [];
		foreach ($teams_data as $team) {
			$teams[$team["team_id"]] = $team;
		}

		$meta = $scores["meta"];
		$data = $scores["results"];

		//
		$main = "";

		//sections
		foreach ($meta as $category) {
			$section = "<section id='".$category["category_name"]."'>";
			$section .= "<h2>".$category["display_name"]."</h2>";

			$table = "<table class='results'>";

			// Order fields
			$order = [];
			// Ignore fields with display_order of -1
			foreach($category["fields"] as $field) {
				if ($field["display_order"] !== -1) {
					$order[] = $field;
				}
			}
			// Sort fields by display_order
			usort($order, function($a, $b) {
				return $a["display_order"] - $b["display_order"];
			});

			// Table headings
			$hdrow = "<tr>";
			$hdrow .= "<th>Ranking</th>";
			$hdrow .= "<th>Team</th>";
			// Category headings
			foreach ($order as $field) {
				$hdrow.= "<th>".$field["display_name"]."</th>";
			}
			$hdrow .= "</tr>";
			$table .= $hdrow;

			// Sort results by place
			$results = [];
			// Transform data to non-assosiative array
			foreach ($data[$category["category_name"]] as $team_id => $team_results) {
				$results[] = ["team_id" => $team_id, "results" => $team_results];
			}
			// Sort (must return int)
			usort($results, function($a, $b) use ($category) {
				$diff = $a["results"][$category["sort_field"]] - $b["results"][$category["sort_field"]];
				if ($category["sort_direction"] === "ASC") {
					if ($diff > 0) {
						return 1;
					} else if ($diff < 0) {
						return -1;
					}
					return 0;
				} else {
					if ($diff > 0) {
						return -1;
					} else if ($diff < 0) {
						return 1;
					}
					return 0;
				}
			});

			// Table body
			foreach ($results as $index => $result) {
				$row = "<tr>";

				// Place
				$row .= "<td>".(string)($index + 1)."</td>";

				// Team
				$row .= "<td>".$teams[$result["team_id"]]["name"]."</td>";

				// Category Fields
				foreach($order as $field) {
					$field_name = $field["field_name"];
					$value = $result["results"][$field_name];

					$row .= "<td>".(string)($value)."</td>";
				}
				$table .= $row;
			}

			$table .= "</table>";
			$section .= $table;

			$section .= "</section>";
			$main .= $section;
		}

		return $main;
	}
}
?>
